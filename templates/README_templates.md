# ZQuery Template Reference

This is a list of macros that you can insert in your template files.  Macros are special 

ZQuery will read the file, and wherever it sees the macro variable, it will replace it with the data from the server.

**IMPORTANT!**  Player-specific macros start with "%player" (%playerName%, %playerPing%, etc.).  When these are encountered, the program will copy the line FOR EACH PLAYER.  It's generally a good idea to keep all the player information on a single line, so it gets copied logically for each player.  You can see these in the predefined template files included in this folder.


## Macro Variables with Description:

%ZVersion%              :   Zandronum version string (including OS info)<br><br>

%serverName%            :   Published server name<br>
%serverURL%             :   Published server URL<br>
%serverEmail%           :   Server E-mail contact<br>
%serverMap%             :   Current map (E1M1, etc.)<br>
%maxPlayers%            :   Max players allowed on server<br>

%serverGame%            :   Main game base (Doom II, Hexen, etc.)<br>
%serverIwad%            :   IWad file (ex. doom2.wad)<br>
%serverSkill%           :   Skill level (ex. Ultra-violence)<br>


%pwadCount%             :   Number of extra wads loaded<br>
%pwadList%              :   List of extra wads loaded (space-separated)<br><br>


%serverType%            :   What kind of Zandronum game is played (CTF, Deathmatch, Cooperative, etc.)<br>
%instaGib% true/false   :   Instagib mode enabled?<br>
%buckShot% true/false   :   Buckshot mode enabled?<br>
%teamGame% true/false   :   Is this game mode a team-based one?<br>



%fragLimit%             :   Kill limit<br>
%timeLimit%             :   Time remaining til next map (0 = infinite)<br>
%timeLeft%              :   Time left til next map<br>
%duelLimit%             :   Duel limit (duel based game modes only)<br>
%pointLimit%            :   Point limit until next map<br>
%winLimit%              :   Win limit until next map<br>

%clientCount%           :   How may players total are connected?<br>

%currentTime%           :   Current time in this ISO format:  YYYY-MM-DD HH:MM <br>
%serverIP%              :   Server IP or hostname, given at command line<br>
%serverPort%            :   Server Port, given at command line<br>


<br>

**Player values:** (one of these found means line is repeated for each player):
<br>

%playerName%            :   Player's name (no colors, sorry)<br>
%playerPing%            :   Player's ping value<br>
%playerScore%           :   Player score<br>
%playerSpectate%        :   Is player spectator? (True/False)
%playerIsBot%           :   Is player a bot? (True/False)
%playerTeamNumber%      :   Which team is player on? 0-3
%playerMinutes%         :   How many minutes has player been on



