#!/usr/bin/env python3


import socket,time,sys
import argparse

from pathlib import Path

# Huffman compression library provided by Alex Mayfield and Teemu Piipo - works a treat!
# (https://bitbucket.org/crimsondusk/pyskull)
import lib.huffman as huffman

import lib.ZCommon as ZCommon

from lib.ZPlayer import *
from lib.ZServer import *
from lib.ZOutput import *




# Parse arguments:
serverAddress = ''

parser = argparse.ArgumentParser(description='Query tool for Zandronum multiplayer Doom servers (formerly Skulltag).  This program will query a given server and produce a list of server information, players, scores, pings, etc.  The output can be customized using template files - HTML and plain text are included by default.')
parser.add_argument('serverAddress', metavar='SERVER:PORT', type=str, nargs=1, help='Server and port to query (ex: localhost:10666)')
parser.add_argument('-d', '--debug', action='store_true', help='Show detailed debug messages to STDERR.  Useful for development or advanced troubleshooting')
parser.add_argument('-t', '--template', help='Custom template file to use for output')
parser.add_argument('-p', '--plain', action='store_true', help='Use included plain text template, as opposed to the default HTML one')
parser.add_argument('-o', '--output', help='Write output to the specified file instead of STDOUT')

args = parser.parse_args()


# Set debug flag globally
debug = bool(args.debug)
ZCommon.debug = debug

if ZCommon.debug == True:
  print("DEBUG :: Options: \nserverAddress   " + str(args.serverAddress[0]), file=sys.stderr)
  print("--debug  " + str(args.debug), file=sys.stderr)
  print("--template  " + str(args.template), file=sys.stderr)
  print("--plain  " + str(args.plain), file=sys.stderr)
  print("--output  " + str(args.output), file=sys.stderr)



# Get the path of our zquery.py script file
# From here, we can tell where our templates/ directory is (with the included default templates)
homeScript =  Path(__file__).resolve().parent


# Set to the included plain.template or html.template by default
if args.plain == True:
  tFile = Path.joinpath(homeScript, "templates", "plain.template")
else:
  tFile = Path.joinpath(homeScript, "templates",  "html.template")


# If user has specified a custom template file, use that instead
if args.template != None:
  tFile = Path(str(args.template))


# Set output file, or "" for none:
if args.output != None:
  outFile = str(args.output)
else:
  outFile = ""



# Parse the server:port argument, but be cautious about it:
try:
  tmp = args.serverAddress[0].split(':')
  port = int(tmp[1])
  address = str(tmp[0])
except:
  print("Error!  You must specify the server in the form SERVER:PORT#  (like zandronum.com:10666)")
  quit(1)


# Create a server object, then query it over the network, then parse the resulting byte stream we receive
server = ZServer(address, port, ZCommon.DEFAULT_FLAGS)
server.QueryServer()
server.ParseData()


# ZOutput reads a template file (tFile), and outputs the file but with macros (% %) replaced with the server/player values
out = ZOutput(server, str(tFile), outFile)


quit()

