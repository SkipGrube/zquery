# ZQuery

ZQuery is a tool for querying [Zandronum](http://zandronum.com) servers.  Zandronum is a multiplayer-oriented Doom source port, formerly known as Skulltag.

This tool is written in standard Python 3, and should work without any extra modules or addons.  It should work fine across platforms, but most testing is done from Linux.


## 0. Target Audience

This program is primarily intended for someone running or hosting a Zandronum server.  It's an easy way to publish real-time information (stats, players connected, etc.) about a running game.  The idea is the stats would be saved as HTML and published to a live website.  Obviously you can use it however you want!

<br>

## 1. Quick Start:

``python3  zquery.py  myserver.mydomain.com:10666  >  /var/www/html/server_status.html``

<br>
The program will connect to a server, gather info, and then output according to a pre-defined template file.  The default is a basic HTML page with some server information and a table of connected players.  A plaintext option is available with the **--plain** flag.
<br>
Custom template files are easy to make, and you're encouraged to customize or create your own!  The default ones are located in the **templates/** folder of the project.  That folder also has a README file that references the different macros available in your template.

<br>

## 2. Options

ZQuery accepts several command line arguments.  Generally, the command is invoked like:  ``python3 zquery.py --option1  --option2  SERVER:PORT``
<br>

Here are the arguments available:

**-d, --debug**                 :   Show detailed debug messages to STDERR. Useful for development or advanced troubleshooting

**-t FILE, --template FILE**    :   Use FILE as the output template instead of a default

**-p, --plain**                 :   Use included plain text template, as opposed to the default HTML one (templates/plain.template)

**-o OUTPUT, --output OUTPUT**  :   Write output to the specified file instead of STDOUT

<br>
<br>



## 3. Template Files

ZQuery template files are just text files with special macros inserted where you want various server information to show up.  Macros are special words surrounded by % %.  For example, **%serverName%** is the title of the server.  **%%maxPlayers%** is the maximum number of players allowed on the server.  Other examples include %playerName%, %serverURL%, etc.  

A few default templates are included under the templates/  folder, but you can customize them or create your own.

When ZQuery runs, it processes a template file line-by-line.  **IMPORTANT**: ANY line where a player value is discovered (%playerName%, %playerPing%, etc.) will be REPEATED FOR EACH PLAYER.  It's important to remember this: generally you want all player information to be on the same line so it gets repeated per-player.  Putting the values on different lines mean you would get all the player names, then all the player pings, etc.  This might not be what you want.


<br>

## 4. About this Program

I'm a fan of Zandronum, I wanted to pick up Python 3 (already familiar with 2), and I wanted to learn a bit about UDP socket programming.  This project was the natural result!  I put it together in my spare time, and it comes with no warranty at all.  I'm releasing the code under a GPLv3 license (see details below).  I hope it's useful to you.

I run a couple of Zandronum servers myself from home, and you can see this program's output in action at their web address:  [http://wolf3d.linuxdn.org](http://wolf3d.linuxdn.org)

**Improvements are always welcome!**  I work in the technology field, but doing this sort of coding is definitely not my day job.  If you have a fix, or an improvement in the code, please share!  Preferably with explanation of what is being fixed or improved upon.

If you have an issue or comment (hopefully good!) you can file an issue in Gitlab, or reach me directly: *team77mail  [at]  yahoo (.) com*

Again, I hope you enjoy!

-Skip


<br>

## 5. License Info

I'm releasing this software under the GNU General Public License, version 3.  Commonly called **GPLv3**.  

You can read the full thing here, if interested: [GPLv3 License](https://www.gnu.org/licenses/gpl-3.0.en.html)


For the uninitiated, the terms are pretty simple:

* This software is offered for free, with source code, and no warranty.  Use it however you want!
* You are free to give this program to anyone else
* You are free to make changes to this project, but you MUST share the modified code with anyone you give it to
* Anyone who gets your modified code can also modify in turn, and also must share their modifications.

<br>

Basically it's open code, and we're sharing any changes we make, and use is completely unrestricted.  Bread-and-butter open source / copyleft.










